# Blocky

Double click to kill the selected session. Result will be posted in the status bar.

Toggle "Blocking" to only display sessions that are blocking or are blocked.



Depends on GTK+3 and [OCILIB](https://github.com/vrogier/ocilib)

For windows you will need [GTK+3](http://www.tarnyko.net/dl/gtk.htm)

## Building

Ensure you have OCILIB, Oracle Insantclient odbc, devel and tools installed.

Use msys2 on windows.

```bash
pacman -S asciidoc autoconf autoconf2.13 autogen automake-wrapper bison dos2unix gettext-devel git gperf help2man intltool lemon libtool libunrar-devel make man-db mingw-w64-i686-glade3 mingw-w64-i686-gcc mingw-w64-i686-glade mingw-w64-i686-pkg-config patchutils pkg-config quilt rcs scons swig texinfo texinfo-tex unrar xmlto
make
```

Linux

```bash
make
```

## Screenshots

![blocky](screenshots/blocky.png)

