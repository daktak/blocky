#include "main.h"

#ifdef __x86_64
extern char _binary_obj_temp_glade_start;
extern char _binary_obj_temp_glade_end;
#else
extern char binary_obj_temp_glade_start;
extern char binary_obj_temp_glade_end;
#endif

guint           db_connected = 1;

app_widgets     *widgets;
guint           id;
OCI_Connection* cn;
gchar           *file_contents;
guint           aq_running;

GMainContext *context;
GThread *thread;
guint           window_mode;
guint           sql_running;

//prevent thread from executing a sql statement if
//another thread has not returned
void wait_for_sql()
{
/*    while (sql_running)
    {
       //g_usleep(500000);
       g_usleep(500);
    }*/
}

void set_sql_running(guint running)
{
    sql_running = running;
}

/**
 * String replace
 */
char* astrrepl(char *str, const char *a, const char *b) {
    for (char *cursor = str; (cursor = strstr(cursor, a)) != NULL;){
        memmove(cursor + strlen(b), cursor + strlen(a), strlen(cursor) - strlen(a) + 1);
        for (int i = 0; b[i] != '\0'; i++)
            cursor[i] = b[i];
        cursor += strlen(b);
    }
    return str;
}

/**
 * Load a file into a char array
 * using g_file for cross platform compatibility
 */
char* read_file(char *filename)
{
    gchar *contents;
    GError *err = NULL;

    g_file_get_contents (filename, &contents, NULL, &err);
    g_assert ((contents == NULL && err != NULL) || (contents != NULL && err == NULL));
    if (err != NULL)
    {
        // Report error to user, and free error
        g_assert (contents == NULL);
        g_error_free (err);
    }
    else
    {
        // Use file contents
        g_assert (contents != NULL);
    }
    return contents;
}

//identify the window that is most appropriate for status bar updates
gboolean err_callback(gpointer user_data)
{
    char *msg = user_data;
    switch (window_mode)
    {
        case 1 :
            gtk_statusbar_pop(widgets->w_sb, id);
            gtk_statusbar_push(widgets->w_sb, id, msg);
            break;
        default :
            gtk_statusbar_pop(widgets->w_mainsb, id);
            gtk_statusbar_push(widgets->w_mainsb, id, msg);
    }
    g_print("%s\n",msg);
    return G_SOURCE_REMOVE;
}

/**
 * Error handling for oracle, update login and suite window statusbar
 */
void err_handler(OCI_Error *err)
{
    db_connected = 0;
    g_print
    (
        "code  : ORA-%05i\n"
        "msg   : %s\n"
        "sql   : %s\n",
        OCI_ErrorGetOCICode(err),
        OCI_ErrorGetString(err),
        OCI_GetSql(OCI_ErrorGetStatement(err))
    );
    gchar *msg = g_strdup_printf("ORA-%05i: %s", OCI_ErrorGetOCICode(err),OCI_ErrorGetString(err));
    GSource *source = g_idle_source_new();
    g_source_set_callback(source, err_callback, msg, NULL);
    g_source_attach(source, context);
    g_source_unref(source);
}

//login success
gboolean login_callback(gpointer user_data)
{
    gchar *db = user_data;
    gtk_statusbar_pop(widgets->w_sb, id);
    gchar *msg = "Connected";
    gtk_statusbar_push(widgets->w_sb, id, msg);
    g_print("%s\n", msg);
    show_main_window(db);
    gtk_widget_hide(widgets->w_login);
    window_mode = 0;
    return G_SOURCE_REMOVE;
}

//try extract password from local file
gchar* read_pass(const gchar *user, const gchar *sid)
{
    gchar *passfilename = g_strdup_printf("blocky_%s_%s.txt",user,sid);
    FILE *file = fopen(passfilename, "r");
    gchar pass[100];
    if (file)
    {
        fscanf(file,"%100s\n", pass);
        fclose(file);
        g_stpcpy(passfilename, pass);
    }
    return passfilename;
}

gboolean env_hasnt_value(gchar* value)
{
    gboolean hasnt_value = TRUE;
    FILE *file = fopen(ENV_FILE, "r");
    gchar sid[20];
    if (file != NULL) {
        while (fscanf(file,"%20s\n", sid) == 1)
        {
            if (g_str_equal(g_strchomp(value),g_strchomp(sid)))
            {
                return FALSE;
            }
        }
        fclose(file);
    }
    return hasnt_value;
}

static gpointer login(gpointer user_data)
{
    db_connected = 1;
    //get user, pass and sid
    const gchar *user;
    user = gtk_entry_get_text(GTK_ENTRY(widgets->w_user));
    const gchar *pass;
    pass = gtk_entry_get_text(GTK_ENTRY(widgets->w_pass));
    const gchar *db;
    db = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widgets->w_db));
    //get location of entry
    gint dbint = gtk_combo_box_get_active(GTK_COMBO_BOX(widgets->w_db));

    //g_print("login %s/%s@%s", user, pass, db);

    if (!OCI_Initialize(err_handler, NULL, OCI_ENV_THREADED))
    {
        return G_SOURCE_REMOVE;
    }

    OCI_Initialize(NULL, getenv("ORACLE_HOME"), OCI_ENV_THREADED);

    if (*user=='\0')
    {
        user = "SYS";
    }
    if (*pass=='\0')
    {
        pass = read_pass(user,db);
    }
    wait_for_sql();
    set_sql_running(1);
    if (g_str_equal(user,"SYS"))
    {
        cn = OCI_ConnectionCreate(db, user, pass, OCI_SESSION_SYSDBA);
    } else {
        cn = OCI_ConnectionCreate(db, user, pass, OCI_SESSION_DEFAULT);
    }

    if (db_connected == 1)
    {
        //if was manually entered, add to file
        if (dbint < 0)
        {
            if (env_hasnt_value(g_strdup_printf("%s\n",db)))
            {
                FILE *file = fopen(ENV_FILE,"a");
                fprintf(file, "%s", g_strdup_printf("%s\n",db));
            }
        }
        GSource *source = g_idle_source_new();
        gchar *db1 = g_strdup_printf("%s", db);
        g_source_set_callback(source, login_callback, db1, NULL);
        g_source_attach(source, context);
        g_source_unref(source);
    }
    set_sql_running(0);
    return NULL;
}

gboolean set_combo(gpointer user_data)
{
    int sidid = GPOINTER_TO_INT(user_data);
    gtk_combo_box_set_active(GTK_COMBO_BOX(widgets->w_db), sidid);
    return G_SOURCE_REMOVE;
}

gboolean add_combo(gpointer user_data)
{
    gchar text[20];
    g_stpcpy(text, user_data);
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (widgets->w_db),text);
    return G_SOURCE_REMOVE;
}

static gpointer read_env_file(gpointer user_data)
{
    //read environment list from flie
    FILE *file = fopen(ENV_FILE, "r");
    gchar sid[20];
    gint sidid = -1;
    GSource *source;
    if (file != NULL) {
        //source = g_idle_source_new();
        while (fscanf(file,"%20s\n", sid) == 1)
        {
            //source = g_idle_source_new();
            //g_source_set_callback(source, add_combo, sid, NULL);
            gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (widgets->w_db),sid);
            /*
            g_source_attach(source, context);
            g_source_unref(source);
            */
            sidid = sidid + 1;
        }
        fclose(file);
    }
    source = g_idle_source_new();
    g_source_set_callback(source, set_combo, GINT_TO_POINTER(sidid), NULL);
    g_source_attach(source, context);
    g_source_unref(source);

    return NULL;
}

/**
 * GTK main initialisation
 */
int main(int argc, char *argv[])
{
    GtkBuilder      *builder;
    GtkWidget       *window;
    widgets = g_slice_new(app_widgets);
    set_sql_running(0);
    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
#ifdef __x86_64
    char*  p = &_binary_obj_temp_glade_start;
#else
    char*  p = &binary_obj_temp_glade_start;
#endif
    window_mode = 1;
    //gtk_builder_add_from_file(builder, "glade/blocky.glade", NULL);
    //building from linked object, not from file
    gtk_builder_add_from_string(builder, p, -1 , NULL);

    //get all elements that are needed for later manipulation
    window = GTK_WIDGET(gtk_builder_get_object(builder, "loginDialog"));
    widgets->w_user = GTK_WIDGET(gtk_builder_get_object(builder, "userEntry"));
    widgets->w_pass = GTK_WIDGET(gtk_builder_get_object(builder, "passEntry"));
    widgets->w_db = GTK_WIDGET(gtk_builder_get_object(builder, "dbSelect"));
    widgets->w_sb = GTK_STATUSBAR(gtk_builder_get_object(builder, "msgStatusBar"));
    widgets->w_login = GTK_WIDGET(gtk_builder_get_object(builder, "loginDialog"));
    widgets->w_main = GTK_WIDGET(gtk_builder_get_object(builder, "main"));
    widgets->w_sessions_ls = GTK_LIST_STORE(gtk_builder_get_object(builder,"sessionsLS"));
    widgets->w_mainsb = GTK_STATUSBAR(gtk_builder_get_object(builder,"mainSB"));
    widgets->w_about = GTK_WIDGET(gtk_builder_get_object(builder, "aboutDialog"));

    //connect signals
    gtk_builder_connect_signals(builder, NULL);

    g_object_unref(builder);

    gtk_widget_show(window);
    g_thread_new(NULL, read_env_file, NULL);
    gtk_main();

    g_slice_free(app_widgets, widgets);
    g_thread_join(thread);
    return 0;
}

/**
 * exit and cleanup db connection
 */
void main_quit()
{
    aq_running = 0;
    OCI_Cleanup();
    gtk_main_quit();
}

// called when window is closed
G_MODULE_EXPORT
void on_loginDialog_destroy()
{
    main_quit();
}

G_MODULE_EXPORT
void on_loginDialog_close()
{
    main_quit();
}

G_MODULE_EXPORT
void on_cancelButton_clicked()
{
    main_quit();
}

/**
 * Use user/pass@sid to connect to DB
 */
G_MODULE_EXPORT
void on_okButton_clicked()
{
    gtk_statusbar_pop(widgets->w_sb, id);
    gchar *msg = g_strdup_printf("Connecting");
    gtk_statusbar_push(widgets->w_sb, id, msg);

    g_print("%s\n", msg);
    g_thread_new(NULL, login, NULL);
}
