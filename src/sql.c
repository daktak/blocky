#include "main.h"

OCI_Resultset* get_sql_result_set(char *sql, GSourceFunc func)
{
    OCI_Statement* st;
    OCI_Resultset* rs;
    wait_for_sql();
    set_sql_running(1);
    st = OCI_StatementCreate(cn);
    OCI_ExecuteStmt(st, sql);
    rs = OCI_GetResultset(st);
    set_sql_running(0);
    if (func)
    { 
        GSource *source = g_idle_source_new();
        g_source_set_callback(source, func, rs, NULL);
        g_source_attach(source, context);
        g_source_unref(source);
    } else
    {
        return rs;
    }
    return NULL;
}

void exec_sql_statement(char *sql, GSourceFunc func)
{
    OCI_Statement* st;
    wait_for_sql();
    set_sql_running(1);
    st = OCI_StatementCreate(cn);
    OCI_Prepare(st, sql);
    OCI_Execute(st);
    OCI_Commit(cn);
    set_sql_running(0);
    if (func)
    {
        GSource *source = g_idle_source_new();
        g_source_set_callback(source, func, NULL, NULL);
        g_source_attach(source, context);
        g_source_unref(source);
    }
}
