#include "main.h"

gboolean blocking = 1;

void show_main_window(gchar *db)
{
    populate_ls();
    const gchar *title = gtk_window_get_title(GTK_WINDOW(widgets->w_main));
    gchar *newtitle = g_strdup_printf("%s: %s",title, db);
    gtk_window_set_title(GTK_WINDOW(widgets->w_main), newtitle);
    gtk_widget_show(widgets->w_main);
}

gboolean kill_callback(gpointer user_data)
{
    gchar *msg = "Session Killed";
    gtk_statusbar_pop(widgets->w_mainsb, id);
    gtk_statusbar_push(widgets->w_mainsb, id, msg);
    return G_SOURCE_REMOVE;
}

gboolean populate_test_callback(gpointer result_set)
{
    GtkListStore *store = GTK_LIST_STORE(widgets->w_sessions_ls);
    gtk_list_store_clear(store);
    OCI_Resultset *rs = result_set;
    GtkTreeIter iter1;
    while (OCI_FetchNext(rs))
    {
        gtk_list_store_append (store, &iter1);
        gtk_list_store_set (store, &iter1,
                COLUMN_SID, OCI_GetInt(rs, 1),
                COLUMN_USERNAME, OCI_GetString(rs, 2),
                COLUMN_USER, OCI_GetString(rs, 3),
                COLUMN_OSUSER, OCI_GetString(rs, 4),
                COLUMN_MODULE, OCI_GetString(rs, 5),
                COLUMN_BSESSION, OCI_GetInt(rs, 6),
                COLUMN_KILL, OCI_GetString(rs, 7),
                COLUMN_SQL, OCI_GetString(rs, 8),
                -1);
    }
    return G_SOURCE_REMOVE;
}

static gpointer populate_windows(gpointer user_data)
{
    char* sql = "select distinct \
        a.sid,a.username,a.schemaname,a.osuser, a.module, a.blocking_session, \
    'alter system kill session '''||a.sid||','||a.serial#||',@'||a.inst_id||'''' as killstring, \
        s.sql_text \
        from gv$session a, \
        gv$session b, \
        gv$sql s \
        where a.status = 'ACTIVE' and \
        a.inst_id = b.blocking_instance\
        and a.sid = b.blocking_session \
        and s.sql_id = a.sql_id \
        union all \
        select distinct \
    a.sid,a.username,a.schemaname,a.osuser, a.module, a.blocking_session, \
    'alter system kill session '''||a.sid||','||a.serial#||',@'||a.inst_id||'''' as killstring, \
        s.sql_text \
        from gv$session a, \
        gv$sql s \
        where a.status = 'ACTIVE'  \
        and s.sql_id = a.sql_id";
    if (blocking == 1)
    {
        char* sql1 = "and blocking_session is not null";
        sql = g_strdup_printf("%s %s",sql,sql1);
    }
    get_sql_result_set(sql, populate_test_callback);
    return NULL;
}

void populate_ls()
{
    thread = g_thread_new(NULL, populate_windows, NULL);
}

G_MODULE_EXPORT
void on_main_destroy()
{
    main_quit();
}

G_MODULE_EXPORT
void on_blocking_toggled()
{
    if (blocking == 0) {
        blocking = 1;
    } else {
        blocking = 0;
    }
    populate_ls();
}

G_MODULE_EXPORT
void on_refresh_clicked()
{
    populate_ls();
}

G_MODULE_EXPORT
void on_sessionsTV_row_activated(GtkTreeView *treeview,
                                GtkTreePath       *path,
                                GtkTreeViewColumn *column,
                                gpointer userdata)
{
    gchar* killsql;
   GtkTreeIter iter;
   GtkTreeModel *model = gtk_tree_view_get_model(treeview);

   if (gtk_tree_model_get_iter(model, &iter, path))
   {
        gtk_tree_model_get (GTK_TREE_MODEL(model), &iter, COLUMN_KILL, &killsql, -1);
        exec_sql_statement(killsql, kill_callback);
        g_free(killsql);
   }
}

G_MODULE_EXPORT
void on_about_clicked()
{
    gtk_widget_show(widgets->w_about);
}

G_MODULE_EXPORT gboolean
on_aboutDialog_delete_event(GtkWidget *w, GdkEvent *e, gpointer u) {
    gtk_widget_hide(w);
    return TRUE;
}

G_MODULE_EXPORT gboolean
on_aboutDialog_response(GtkWidget *w, GdkEvent *e, gpointer u) {
    gtk_widget_hide(w);
    return TRUE;
}
