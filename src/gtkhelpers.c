#include "main.h"

gchar* get_selected_column(GtkTreeView *tv, gint column, gboolean remove)
{

    gchar *value = "";
    GtkTreeSelection *selection;
    GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(tv));
    selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(tv));
    GtkTreeIter iter;
    gtk_tree_selection_get_selected(selection, &model, &iter);
    gchar *iter_char = gtk_tree_model_get_string_from_iter(model, &iter);
    if (iter_char)
    {
        gtk_tree_model_get (GTK_TREE_MODEL(model), &iter, column, &value, -1);
        if (remove)
        {
            if (!(gtk_tree_store_remove(GTK_TREE_STORE(model), &iter)))
            {
                //gtk_tree_model_get (GTK_TREE_MODEL(model), &iter, column, &value);
                gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
            }
        }
    }
    g_free(iter_char);
    return value;
}
