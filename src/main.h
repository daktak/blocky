/*
 * main.h
 * Copyright (C) 2018 user <user@work>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef MAIN_H
#define MAIN_H

#include <gtk/gtk.h>
#include <stdio.h>
#include <ocilib.h>

#define ENV_FILE "envs.cfg"

typedef struct {
    GtkWidget *w_user;
    GtkWidget *w_pass;
    GtkWidget *w_db;
    GtkStatusbar *w_sb;
    GtkWidget *w_login;
    GtkWidget *w_main;
    GtkListStore *w_sessions_ls;
    GtkStatusbar *w_mainsb;
    GtkWidget *w_about;
} app_widgets;

void main_quit();
void err_handler(OCI_Error *err);
char* read_file(char *filename);

extern void show_main_window(gchar *db);
extern void set_sql_running(guint running);
extern void wait_for_sql();
extern OCI_Resultset* get_sql_result_set(char *sql, GSourceFunc func);
extern void populate_ls();
extern gchar* get_selected_column(GtkTreeView *tv, gint column, gboolean remove);
extern void exec_sql_statement(char *sql, GSourceFunc func);

enum
{
    COLUMN_SID,
    COLUMN_USERNAME,
    COLUMN_USER,
    COLUMN_OSUSER,
    COLUMN_MODULE,
    COLUMN_BSESSION,
    COLUMN_KILL,
    COLUMN_SQL
};

extern app_widgets     *widgets;
extern guint           id;
extern OCI_Connection* cn;
extern gchar           *file_contents;
extern guint           aq_running;

extern GMainContext *context;
extern GThread *thread;
extern guint           window_mode;
extern guint           sql_running;

#endif /* !MAIN_H */
