ARCH := $(shell getconf LONG_BIT)
GTKLIB=`pkg-config --cflags --libs gtk+-3.0 gmodule-export-2.0`
SED     := sed
OBJCOPY := objcopy
MKDIR   := mkdir
RMDIR   := rm -rf
CC      := gcc
BIN     := ./bin
OBJ     := ./obj
SRC     := ./src
SRCS    := $(wildcard $(SRC)/*.c)
OBJS    := $(patsubst $(SRC)/%.c,$(OBJ)/%.o,$(SRCS))
EXE     := $(BIN)/blocky
PTHREAD := -pthread

ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

ifeq ($(OS),Windows_NT)
    ORACLE_HOME := `cd ${ORACLE_HOME}; pwd`
    OCILIB_HOME := /c/ocilib
    LDLIBS  := $(PTHREAD) $(GTKLIB) -export-dynamic -L${ORACLE_HOME}/sdk/lib/msvc -L${OCILIB_HOME}/lib${ARCH} -lociliba -loci -mwindows
    CFLAGS  += -I${OCILIB_HOME}/include -DOCI_API=__stdcall -mwindows  -Wno-error=deprecated-declarations
    EXE     := $(BIN)/blocky.exe
else
    LDLIBS  := $(PTHREAD) $(GTKLIB) -lm -L$(ORACLE_HOME)lib/ -lclntsh /usr/local/lib/libocilib.a
    CFLAGS  := -I$(INCLUDE) -O2 -Wall -g -Werror -std=c99
endif
INCLUDE := $(ORACLE_HOME)
ELF_64  := elf64-x86-64
ELF_32  := elf32-i386
ELF := $(ELF_$(ARCH))
ARCH_FLAG_64 := i386:x86-64
ARCH_FLAG_32 := i386
ARCH_FLAG := $(ARCH_FLAG_$(ARCH))

.PHONY: all run clean install

all: $(EXE)

$(EXE): $(OBJS) | $(BIN) glade.o
	    $(CC) $(LDFLAGS) $^ -o $@ obj/glade.o $(LDLIBS)

$(OBJ)/%.o: $(SRC)/%.c | $(OBJ)
	    $(CC) $(CFLAGS) -c $< $(GTKLIB) -o $@

$(BIN) $(OBJ):
	    $(MKDIR) $@

glade.o: $(OBJ)
	$(SED) 's/<property name="version"> <\/property>/<property name="version">'`git tag | tail -n1`'<\/property>/i' glade/blocky.glade > obj/temp.glade
	$(OBJCOPY) --input-target binary --output-target $(ELF) --binary-architecture $(ARCH_FLAG) obj/temp.glade obj/glade.o

run: $(EXE)
	    $<

clean:
	    $(RMDIR) $(OBJ)
	    rm ${EXE}

install:
	install -d $(DESTDIR)$(PREFIX)/bin/
	install -m 755 $(EXE) $(DESTDIR)$(PREFIX)/bin/
	install -d $(DESTDIR)$(PREFIX)/share/applications/
	install -m 644 resources/blocky.desktop $(DESTDIR)$(PREFIX)/share/applications/
